<?php


use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Assert;

class StorageHandlerTest extends TestCase
{
    private $storageHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storageHandler = new StorageHandler();
    }

    public function testGetFullFreeCapacity(){
        $this->storageHandler->addNewStorage("First", "Budapest", 100);
        $this->storageHandler->addNewStorage("Second", "Budapest", 200);
        $expected = 300;
        $actual = $this->storageHandler->getFullFreeCapacity();
        Assert::assertSame($expected, $actual, "Jujj");
    }

    public function testTooMuchProduct(){
        $this->storageHandler->addNewStorage("First", "Budapest", 100);
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");
        $actual = "";
        try {
            $this->storageHandler->transportToStorage([["product" => $shoes1, "quantity" => 200]]);
        }catch (Exception $e){
            $actual = $e->getMessage();
        }
        $expected = "Too much product!";

        Assert::assertSame($expected, $actual, "jujj");
    }

    public function testFreeCapacityAfterAdding(){
        $this->storageHandler->addNewStorage("First", "Budapest", 100);
        $this->storageHandler->addNewStorage("Second", "Pécs", 200);
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storageHandler->transportToStorage([["product" => $shoes1, "quantity" => 250]]);
        $actual = $this->storageHandler->getFullFreeCapacity();
        $expected = 50;

        Assert::assertSame($expected, $actual, "jujj");
    }

    public function testContentOfStorages(){
        $this->storageHandler->addNewStorage("First", "Budapest", 100);
        $this->storageHandler->addNewStorage("Second", "Pécs", 200);
        $this->storageHandler->addNewStorage("Second", "Győr", 150);
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");
        $shoes2 = new Shoes(1, $nike, "00-000-02", "Air Zoom Pegazus 34", 99, 7, "white");
        $polar = new Brand(2, "Polar", 1);
        $watch1 = new Watch(1, $polar, "00-000-02", "", 143, "black", "M400");
        $transport = [
            ["product" => $shoes1, "quantity" => 100],
            ["product" => $watch1, "quantity" => 80],
            ["product" => $shoes2, "quantity" => 130],
        ];
        $this->storageHandler->transportToStorage($transport);
        //$actual = $this->storageHandler->getContentOfStorages();
        $actual = $this->storageHandler->getFullFreeCapacity();
        $expected = 140;

        Assert::assertSame($expected, $actual, "jujj");
    }

    public function testTransportFromStorages(){
        $this->storageHandler->addNewStorage("First", "Budapest", 100);
        $this->storageHandler->addNewStorage("Second", "Pécs", 200);
        $this->storageHandler->addNewStorage("Second", "Győr", 150);
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");
        $this->storageHandler->transportToStorage([["product" => $shoes1, "quantity" => 380]]);
        $this->storageHandler->transportFromStorage($shoes1, 330);
        $actual = $this->storageHandler->getFullFreeCapacity();
        $expected = 400;

        Assert::assertSame($expected, $actual, "jujj");
    }
}
