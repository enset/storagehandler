<?php


use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Assert;

class StorageTest extends TestCase
{
    private $storage;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storage = new Storage(1, "Big storage", "Budapest", 100);
    }

    public function testFreeCapacityOfStorage(){
        $actual = $this->storage->getFreeCapacity();
        $expected = 100;

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }

    public function testGetContentOfStorage(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 10);
        $expected = ["0" => ["product" => $shoes1, "quantity" => 10]];
        $actual = $this->storage->getProducts();

        Assert::assertSame($expected, $actual, "Hopp!!!");
    }

    public function testGetFreeCapacityAfterAdding(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 10);
        $expected = 90;
        $actual = $this->storage->getFreeCapacity();

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }

    public function testGetContentAfterAddingSameProductTwice(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 10);
        $this->storage->addProducts($shoes1, 20);
        $expected = ["0" => ["product" => $shoes1, "quantity" => 30]];
        $actual = $this->storage->getProducts();

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }

    public function testAddProductIfStorageIsFull(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 100);
        $actual = $this->storage->addProducts($shoes1, 10);
        $expected = false;

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }

    public function testRemoveProductFromStorage(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 100);
        $actual = $this->storage->removeProducts($shoes1, 110);
        $expected = 100;

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }

    public function testRemoveProductFromStorage2(){
        $nike = new Brand(1, "Nike", 1);
        $shoes1 = new Shoes(1, $nike, "00-000-01", "Air Zoom Pegazus 35", 99, 8, "blue");

        $this->storage->addProducts($shoes1, 100);
        $this->storage->removeProducts($shoes1, 110);
        $actual = $this->storage->getProducts();
        $expected = [];

        Assert::assertSame($expected, $actual, "Something went wrong.");
    }
}
