<?php


class Product
{
    private $id;
    private $brand;
    private $productNumber;
    private $productName;
    private $price;

    public function __construct(int $id, Brand $brand, string $productNumber, string $productName, float $price){
        $this->id = $id;
        $this->brand= $brand;
        $this->productNumber = $productNumber;
        $this->productName = $productName;
        $this->price = $price;
    }
}