<?php


class T_Shirt extends Product
{
    private $size;
    private $color;
    private $material;

    public function __construct(int $id, Brand $brand, string $productNumber, string $productName, float $price, int $size, string $color, string $material)
    {
        parent::__construct($id, $brand, $productNumber, $productName, $price);
        $this->size = $size;
        $this->color = $color;
        $this->material = $material;
    }

}