<?php


class Storage
{
    private $id;
    private $name;
    private $address;
    private $capacity;
    private $products;

    public function __construct(int $id, string $name, string $address, int $capacity){
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
        $this->products = [];
    }

    public function getFreeCapacity():int{
        $storedQuantity = 0;
        foreach ($this->products as $product){
            $storedQuantity += $product["quantity"];
        }
        return $this->capacity-$storedQuantity;
    }

    public function addProducts(Product $product, int $quantity):bool{
        if (($this->getFreeCapacity() - $quantity) >= 0){
            foreach ($this->products as &$productItem){
                if ($productItem["product"] === $product){
                    $productItem["quantity"] += $quantity;
                    return true;
                }
            }
            $this->products[] = ["product" => $product, "quantity" => $quantity];
            return true;
        }
        return false;
    }

    public function removeProduct(Product $product, int $quantity):int{
        foreach ($this->products as $index => &$productItem) {
            if ($productItem["product"] === $product) {
                $removedQuantity = $quantity > $productItem["quantity"] ? $productItem["quantity"] : $quantity;
                $productItem["quantity"] -= $removedQuantity;
                if ( $productItem["quantity"] === 0){
                    array_splice($this->products, $index, 1);
                }
                return $removedQuantity;
            }
        }
        return 0;
    }

    public function getProducts():array{
        return $this->products;
    }

    public function getName():string{
        return $this->name;
    }

    private function searchProduct(Product $product):?array{

        return null;
    }
}