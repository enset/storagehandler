<?php


class Watch extends Product
{
    private $color;
    private $type;

    public function __construct(int $id, Brand $brand, string $productNumber, string $productName, float $price, string $color, string $type)
    {
        parent::__construct($id, $brand, $productNumber, $productName, $price);
        $this->color = $color;
        $this->type = $type;
    }
}