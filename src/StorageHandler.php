<?php


class StorageHandler
{
    private $storages = [];

    public function addNewStorage(string $name, string $address, int $capacity){
        $id = count($this->storages);
        $this->storages[] = new Storage(++$id, $name, $address, $capacity);
    }

    public function getFullFreeCapacity():int{
        $fullCapacity = 0;
        foreach ($this->storages as $storage){
            $fullCapacity += $storage->getFreeCapacity();
        }
        return $fullCapacity;
    }

    public function transportToStorage(array $products){
        if ($this->getFullFreeCapacity() < $this->getFullTransportQuantity($products)){
            throw new Exception("Too much product!");
        }
        foreach ($products as $product){
            foreach ($this->storages as $storage){
                if (($freeCapacity = $storage->getFreeCapacity()) > 0){
                    $storedQuantity = $freeCapacity >= $product["quantity"] ? $product["quantity"] : $freeCapacity;
                    $storage->addProducts($product["product"], $storedQuantity);
                    if (($product["quantity"] -= $storedQuantity) == 0){
                        break;
                    }
                }
            }
        }
    }

    public function transportFromStorage(Product $product, int $quantity):int{
        $removedQuantity = 0;
        foreach ($this->storages as &$storage){
            $removedQuantity += $storage->removeProduct($product, $quantity-$removedQuantity);
            if ($quantity == $removedQuantity){
                break;
            }
        }
        return $removedQuantity;
    }

    public function getContentOfStorages():array{
        $content = [];
        foreach ($this->storages as $storage){
            $content[] = ["storageName" => $storage->getName(), "content" => $storage->getProducts()];
        }
        return $content;
    }

    private function getFullTransportQuantity(array $products):int{
        $fullQuantity = 0;
        foreach ($products as $product){
            $fullQuantity += $product["quantity"];
        }
        return $fullQuantity;
    }
}