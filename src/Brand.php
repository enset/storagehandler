<?php


class Brand
{
    private $id;
    protected $brandName;
    protected $qualityCategory;

    public function __construct(int $id, string $brandName, int $qualityCategory){
        $this->id = $id;
        $this->brandName = $brandName;
        $this->qualityCategory = $qualityCategory;
    }
}